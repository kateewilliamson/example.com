<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'database_example');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '|o;i}.lM3;+$Cnls!npLU.X r~-zj(0)kVvjUC&p{PY~tN-]/yS*zelvU/Xzp`]!');
define('SECURE_AUTH_KEY',  'j:)LGw=vT(s6$K_*g#sH`(!kbr-WU1>)&T/P+1P_qAE3Ksmzx47%u3V$;3Vhu dz');
define('LOGGED_IN_KEY',    '1B2msMPab|-^Bp|t8v7@O]Q*;AqiUQTC; )GPj>$N+|YNbsEBtw7y&cmYM`$Q|HJ');
define('NONCE_KEY',        '<D-%[}iI|xN:2u3+z}!HyzL,Kr9ukjSrQTDX#Kyb?sy?Ber~W4E2B?,/u2$8B&+d');
define('AUTH_SALT',        '@O7*#(}rIQ$=YeqW72n$RN5iYk^p_mw8z9;dX=hIxon$Bomy]:_kCt|VDFwpYAhm');
define('SECURE_AUTH_SALT', 'BP^WxC_l$|.U|r`z|vsts>g#,rDs5GnAwN|VcmB;YtazFUY$9p2(kb24-rVc$f;S');
define('LOGGED_IN_SALT',   'F0|)mfJ^ X}487dTCz5.s/b$c%?,hT(+-CJniG@oO@nP+~czOc{L?6{OBAL?;|~|');
define('NONCE_SALT',       'c8,ONs2RqWw)iPq#w7w+m}9sDQXS?{qiK(o WAx.:5x6`]|0>*u^o1$oBZ{PvcS?');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'exmpl_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
